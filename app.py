import gi
import json

gi.require_version("Gtk", "3.0")


from gi.repository import Gtk

class ExceptionErrors:
    """
    A simple class-of-classes containing custom exception
    """

    class ConfigNotFound(BaseException):
        """
        Used for "Config could not be found/accessed!"
        """

        pass


class Config:
    """
    Used for the basic configs of sc-convert
    """

    class BaseConfig:
        """
        Used for the essential need-to-know consts that are used universially
        """

        CONFIG_FILE = "config.json"

    def get_config(self):
        with open(self.BaseConfig().CONFIG_FILE, "r") as config_in:
            try:
                return json.load(config_in)
            except:
                raise ExceptionErrors.ConfigNotFound(
                    "Config could not be found/accessed")


class FFMPEGWrapper:
    """
    This class is a simple wrapper on the FFMPEG python module
    Using this makes it easier to control what happens when we want to do x
    """

    pass


class MainWindow(Gtk.Window):
    config = Config().get_config()
    media_filename = ""

    def __init__(self, title="scConvert"):
        Gtk.Window.__init__(self, title="scConvert")

        self.set_border_width(5)

        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        self.add(main_box)

        edit_tabs = Gtk.Notebook(expand=True)
        main_box.add(edit_tabs)

        page_convert = Gtk.Box()
        page_convert.set_border_width(10)
        edit_tabs.append_page(page_convert, Gtk.Label("Converting"))

        self.add_convert_buttons(page_convert)

        bottom_box = Gtk.Box(spacing=5)
        main_box.add(bottom_box)

        bottom_file = Gtk.Button("Choose File..", expand=True)
        bottom_file.connect("clicked", self.get_filepicker)
        bottom_box.add(bottom_file)

        bottom_output_name = Gtk.Entry(expand=True)
        bottom_output_name.set_text("Output Name")
        bottom_box.add(bottom_output_name)

    def add_convert_buttons(self, page_convert):
        """
        Adds the convert buttons whilst making sure filetypes align
        """

        for ext in self.config["tabs"]["convert"]["allowed_ext"]:
            if ext == self.media_filename: # TODO make this work properly (self.media_filename)
                ext_button = Gtk.Button(f"To .{ext}..") # TODO make this disabled
            else:
                ext_button = Gtk.Button(f"To .{ext}..")
            
            page_convert.add(ext_button)

    def get_filepicker(self, widget):
        """
        Makes a simple filepicker dialouge
        """

        dialog = Gtk.FileChooserDialog(
            "Please choose a file",
            self,
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK
            )
        )

        self.add_filepicker_filter(dialog)

        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            self.media_filename = dialog.get_filename()

        dialog.destroy()

    def add_filepicker_filter(self, dialog):
        """
        Adds all the filters for the filepicker, using config.json
        for all of the video extentions
        """

        filter_media = Gtk.FileFilter()
        filter_media.set_name("Video files")

        for pattern in self.config["video_ext"]:
            filter_media.add_pattern(f"*.{pattern}")

        dialog.add_filter(filter_media)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)


main_win = MainWindow()
main_win.connect("destroy", Gtk.main_quit)
main_win.show_all()

Gtk.main()
