# scConvert

## Overview

### About scConvert

scConvert is a lightweight video utility program that can convert formats & do other secondary items like flipping your video or reversing it!

scConvert is essentially FFMPEG built into an easy-to-use, modern GTK gui based around PyGObject; a Python wrapper for GTK. It is licensed under *Apache 2.0*.

## Installing

To run scConvert, you can simply type in:

```bash
pipenv run python3 app.py
```

Please note that you will have to have Python 3.6+ installed along with the `pipenv` package.
